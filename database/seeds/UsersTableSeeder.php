<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //admin user
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('secret'),
            'admin' => 1,
        ]);

        for ($i = 1; $i < 21; $i++) {
            DB::table('users')->insert([
                'name' => 'user ' .$i,
                'email' => 'user_' .$i.'@gmail.com',
                'password' => bcrypt('secret'),
            ]);
        }
    }
}
