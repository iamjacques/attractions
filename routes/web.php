<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainCtrl@index');
Route::get('attraction/{id}', 'MainCtrl@attraction');
Route::get('top-5', 'MainCtrl@top5');

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::post('/add/review/{id}', 'HomeController@createReview');



Route::get('/edit/attractions', 'AdminCtrl@index');
Route::get('/edit/attraction/{id}', 'AdminCtrl@edit');
Route::post('/edit/attraction/{id}', 'AdminCtrl@postEdit');
Route::get('/delete/attraction/{id}', 'AdminCtrl@deleteAttraction');
Route::get('/delete/review/{id}', 'AdminCtrl@deleteReview');
Route::get('/disable/review/{id}', 'AdminCtrl@disableReview');


