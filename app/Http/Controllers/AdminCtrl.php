<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\RedirectResponse;


class AdminCtrl extends Controller
{
	public function __construct()
    {
        $this->middleware('admin');
    }

    public function index(){
    	$attractions = \App\Attraction::with('reviews')->get();
    	return view('admin.manage')->with('attractions', $attractions->toArray());
    }

    public function edit($id){
    	 $attraction = \App\Attraction::find($id);
    	$reviews = $attraction->reviews;
    	return view('admin.update')->with('attraction', $attraction->toArray())
    							->with('reviews', $reviews->toArray());
    }

    public function postEdit($id){
    	$attraction = \App\Attraction::find($id);
    	$attraction->title = Input::get('title');
    	$attraction->image = Input::get('image');
    	$attraction->description = Input::get('description');
    	if($attraction->save()){
    		\Session::flash('success',false);
			return redirect()->back();
    	} 
    	\Session::flash('success',false);
    	return redirect()->back();
    }

    public function deleteAttraction($id){
    	 $attraction = \App\Attraction::find($id)->delete();
		return redirect()->back();
    }

    public function deleteReview($id){
    	 $review = \App\Review::find($id)->delete();
		return redirect()->back();
    }

    public function disableReview($id){
    	 $review = \App\Review::find($id);
    	  $review->active = ($review->active == 1) ? 0 : 1;
    	  $review->save();
    	  //return $review;
		return redirect()->back();
    }
}
