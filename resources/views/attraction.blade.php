@extends('layouts.app')

@section('content')
<section class="hero  is-large" style=" background-image: url({{strstr($attraction['image'], '?', true)}}); background-position: center top; background-size: 100% auto;">
  <div class="hero-body" >
    <div class="container">
    </div>
  </div>
</section>

<section class="section" >
  <div class="container">
    <h1 class="title">
        {{$attraction['title']}}
      </h1>
    <p>{{$attraction['description']}} </p>
</div>
</section>

 <section class="section">
    <div class="container">
      <h1 class="title">Reviews</h1>
      @foreach($reviews as $review)
       @if($review['active'])
      <article class="media">
  <figure class="media-left">
    <p class="image is-64x64">
      <img src="http://bulma.io/images/placeholders/128x128.png">
    </p>
  </figure>
  <div class="media-content">
    <div class="content">
      <p>
        <strong>{{$review['user']['name']}}</strong> 
        <br>
        {{$review['description']}}
       
      </p>
    </div>
    <nav class="level is-mobile">
      <div class="level-left">
        @for ($i = 0; $i < $review["rating"]; $i++)
            <a class="level-item">
              <span class="icon is-small"><i class="fa fa-heart"></i></span>
            </a>
        @endfor
         @for ($i = 0; $i < 5-$review["rating"]; $i++)
           <a class="level-item">
              <span class="icon is-small"><i class="fa fa-heart-o"></i></span> 
            </a>
        @endfor

        
      </div>
    </nav>
  </div>
  <div class="media-right">
     <!-- <a href="/delete/attraction/{{$attraction['id']}}" class="delete"></a> -->
  </div>
</article>
@endif
	@endforeach

<br>
<br>
 @if(Auth::check())
{{Form::open(['url' => '/add/review/'.$attraction['id'] , 'method' => 'post']) }}
	<article class="media">
  <figure class="media-left">
    <p class="image is-64x64">
      <img src="http://bulma.io/images/placeholders/128x128.png">
    </p>
  </figure>
  <div class="media-content">
    <div class="field review_text_holder">
      <p class="control">
        <textarea class="textarea" name="description" placeholder="Add a comment..."></textarea>
      </p>
    </div>
    <nav class="level">
      <div class="level-left">
        <div class="level-item">
          <button  class="button is-info create_review">Submit</button>
        </div>
      </div>
       <div class="level-right">
        <div class="level-item ratings">
          <input type="hidden" name="rating" value="1">
           @for ($i = 1; $i < 6; $i++)
           <a class="level-item">
                <span data-rating='{{$i}}' class="icon is-small"><i class="fa fa-heart-o"></i></span> 
            </a>
          @endfor
        </div>
      </div>
    </nav>
  </div>
</article>
{{Form::close() }}
@endif
    </div>
  </section>

@endsection
