@extends('layouts.app') @section('content')

<section class="section">
    <div class="container">
     <h1 class="title">Edit Attraction</h1>
     @if(Session::has('success'))
     <div class="notification is-primary">
        <p> Attraction Updated! </p>
      </div>
      @endif
      {{Form::open(['url' => '/edit/attraction/'.$attraction['id'] , 'method' => 'post']) }}
        <div class="field">
            <label class="label">Name</label>
            <div class="control">
                <input class="input" type="text" name="title" placeholder="Name"  value="{{$attraction['title']}}">
            </div>
        </div>

        <div class="field">
            <label class="label">Image Url</label>
            <div class="control">
                <input class="input" type="text" name="image" placeholder="Image url" value="{{$attraction['image']}}">
            </div>
        </div>

        <div class="field">
            <label class="label">Description</label>
            <div class="control">
                <textarea class="textarea"  name="description" placeholder="Textarea">{{$attraction['description']}}</textarea>
            </div>
        </div>

        <div class="field is-grouped">
            <div class="control">
                <button class="button is-primary">Submit</button>
            </div>
        </div>
        {{ Form::close() }}

    </div>
</section>

<section class="section">
    <div class="container">
        <div class="columns">

            <div class="column ">
                @foreach($reviews as $review)
                <article class="media">
                    <figure class="media-left">
                        <p class="image is-64x64">
                            <img src="http://bulma.io/images/placeholders/128x128.png">
                        </p>
                    </figure>
                    <div class="media-content">
                        <div class="content">
                            <p>
                                <strong>John Smith</strong> <small>31m</small>
                                <br> {{$review['description']}}

                            </p>
                        </div>
                        <nav class="level is-mobile">
                            <div class="level-left">
                                @for ($i = 0; $i
                                < $review[ "rating"]; $i++) <a class="level-item">
                                    <span class="icon is-small"><i class="fa fa-heart"></i></span>
                                    </a>
                                    @endfor @for ($i = 0; $i
                                    < 5-$review[ "rating"]; $i++) <a class="level-item">
                                        <span class="icon is-small"><i class="fa fa-heart-o"></i></span>
                                        </a>
                                        @endfor

                            </div>
                        </nav>
                    </div>
                    <div class="media-right">
                      @if($review['active'])
                      <a href="/disable/review/{{$review['id']}}" class="fa fa-eye-slash"> Disable </a>
                      @else
                      <a href="/disable/review/{{$review['id']}}" class="fa fa-eye"> Unable </a>
                      @endif
                      <a href="/delete/review/{{$review['id']}}" class="fa fa-trash-o"> Delete</a>
                    </div>
                </article>
                @endforeach

            </div>

        </div>
    </div>
</section>

@endsection