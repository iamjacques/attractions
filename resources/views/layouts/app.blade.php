<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>


      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.5.3/css/bulma.min.css">
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
</head>
<body>
        <nav class="navbar is-transparent">
        <div class="navbar-brand">
            <a class="navbar-item" href="/">
                <img src="http://bulma.io/images/bulma-logo.png" alt="" width="112" height="28">
            </a>
            <div class="navbar-burger burger" data-target="navMenuTransparent">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <div id="navMenuTransparent" class="navbar-menu">
            <div class="navbar-start">
                <a class="navbar-item " href="/">
               Attractions
               </a>
                <a class="navbar-item " href="/top-5">
               Top 5
               </a>
            </div>
            <div class="navbar-end">
                <div class="navbar-item">
                    <div class="field is-grouped">

                         @guest
                        <p class="control">
                            <a class="bd-tw-button button" href="{{ route('login') }}">
                                <span>
                        Login
                        </span>
                            </a>
                        </p>

                        <p class="control">
                            <a class="button is-primary" href="{{ route('register') }}">

                                <span>Register</span>
                            </a>
                        </p>

                        @else
                        
                        <p class="control">
                            <a class="bd-tw-button button" href="{{ url('/home') }}">
                                <span>
                        Home
                        </span>
                            </a>
                        </p>
                        @if(Auth::user()->admin)
                        <p class="control">
                            <a class="bd-tw-button button" href="{{ url('/edit/attractions') }}">
                                <span>
                        Edit Attractions
                        </span>
                            </a>
                        </p>
                        @endif


                        <div class="navbar-item has-dropdown is-hoverable">
                            <a class="navbar-link  is-active" href="#">
                              {{Auth::user()->name}}
                            </a>
                            <div class="navbar-dropdown is-boxed">
                              <a class="navbar-item "  href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                              </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                         
                            </div>
                          </div>
                          @endguest
                    
                                                
                    </div>
                </div>
            </div>
        </div>
    </nav>

        @yield('content')
 
    <!-- Scripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
